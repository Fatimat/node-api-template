module.exports = ({ config, swaggerUi: { serve, setup } }) => {
  const { NODE_ENV } = config;
  const { DOCUMENTATION } = config.ENDPOINTS;

  return function load(app, { swaggerDocument }) {
    // Load document endpoint
    if (NODE_ENV !== 'production' && swaggerDocument) {
      app.use(DOCUMENTATION, serve, setup(swaggerDocument));
    }
  };
};
