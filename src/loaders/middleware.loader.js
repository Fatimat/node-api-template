module.exports = ({
  bodyParser, compression, cors, cookieParser, config,
}) => function load(app) {
  // Load useful middlewares
  app.use(cookieParser());
  // parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: false }));
  // parse application/json
  app.use(bodyParser.json());
  app.use(cors(config.SERVER.CORS_OPTIONS));
  app.use(compression());
};
