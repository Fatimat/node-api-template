module.exports = ({
  express, httpTerminator, config, dummyController, middlewaresLoader, documentationLoader,
}) => {
  const {
    ENDPOINTS: { DUMMY },
  } = config;
  let terminator = null;
  function run(app, port) {
    return new Promise((resolve, reject) => {
      if (!port) {
        return reject(new Error('Cannot start server without port option'));
      }
      const server = app.listen(port, () => {
        console.log('Server running on port:', port);
        resolve();
      });
      terminator = httpTerminator.createHttpTerminator({ server });
      return null;
    });
  }

  return {
    async start(opts) {
      // Create Express server
      const app = express();
      middlewaresLoader(app);
      documentationLoader(app, opts);
      // Run the server
      await run(app, opts.port);
      // ENDPOINTS
      app.get(DUMMY, dummyController.sendDummyData);
      return app;
    },
    async stop() {
      // Terminate server process
      await terminator.terminate();
    },
  };
};
