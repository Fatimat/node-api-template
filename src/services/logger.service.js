module.exports = ({ winston, config }) => {
  const { LOG_LEVEL } = config;
  const { createLogger, transports, format } = winston;
  const { combine, prettyPrint, timestamp } = format;
  const logger = createLogger({
    level: LOG_LEVEL,
    format: combine(timestamp(), prettyPrint()),
    transports: [new transports.Console()],
  });

  return logger;
};
