// External Modules
const { createContainer, asValue, asFunction } = require('awilix');
const bodyParser = require('body-parser');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const express = require('express');
const httpTerminator = require('http-terminator');
const winston = require('winston');
const swaggerUi = require('swagger-ui-express');
// Configuration
const config = require('./config');
// Loaders
const middlewaresLoader = require('./loaders/middleware.loader');
const documentationLoader = require('./loaders/swagger.loader');
// Controllers
const dummyController = require('./controllers/dummy.controller');
// Services
const serverService = require('./services/server.service');
const loggerService = require('./services/logger.service');
// DI container
const container = createContainer();

container.register({
  // External Modules
  bodyParser: asValue(bodyParser),
  compression: asValue(compression),
  cookieParser: asValue(cookieParser),
  cors: asValue(cors),
  express: asValue(express),
  httpTerminator: asValue(httpTerminator),
  winston: asValue(winston),
  swaggerUi: asValue(swaggerUi),
  // Configuration
  config: asValue(config),
  // Loaders
  middlewaresLoader: asFunction(middlewaresLoader).singleton(),
  documentationLoader: asFunction(documentationLoader).singleton(),
  // Controllers
  dummyController: asFunction(dummyController).singleton(),
  // Service
  serverService: asFunction(serverService).singleton(),
  loggerService: asFunction(loggerService).singleton(),
});

module.exports = container;
