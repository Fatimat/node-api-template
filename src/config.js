const { NODE_ENV, LOG_LEVEL } = process.env;

module.exports = {
  NODE_ENV,
  LOG_LEVEL,
  SERVER: {
    CORS_OPTIONS: {
      origin: true,
      credentials: true,
    },
  },
  ENDPOINTS: {
    DUMMY: '/dummy',
    DOCUMENTATION: '/swagger-docs',
  },
  STATUS_CODES: {
    OK: 200,
  },
};
