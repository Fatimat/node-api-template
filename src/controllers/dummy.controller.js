module.exports = ({ config, loggerService }) => {
  const {
    STATUS_CODES: { OK },
  } = config;
  function sendDummyData(req, res) {
    loggerService.log({ level: 'info', message: 'running' });
    res.status(OK).send({ dummyEndpoint: true });
  }

  return { sendDummyData };
};
