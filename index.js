require('dotenv').config();

const container = require('./src/container');

const serverService = container.resolve('serverService');

// Specify the Swagger document
const swaggerDocument = require('./doc/swagger.json');

serverService.start({ port: process.env.PORT, swaggerDocument });
