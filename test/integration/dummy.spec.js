const request = require('supertest');
const { resolve } = require('./container');

// Services
const { start, stop } = resolve('serverService');

describe('GET /user', () => {
  let app;
  beforeAll(async () => {
    app = await start({ port: 4000, swaggerDocument: '' });
  });

  afterAll(async () => {
    await stop();
  });

  it('responds with json', (done) => {
    request(app).get('/dummy').set('Accept', 'application/json').expect('Content-Type', /json/)
      .expect(200, done);
  });
});
