const { createContainer } = require('awilix');
// API Container
const apiContainer = require('../../src/container');

// Test Container
const testContainer = createContainer();
testContainer.register(apiContainer.registrations);

module.exports = testContainer;
