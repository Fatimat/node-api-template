# node-api-template

## Code formatting checks

ESLint does automated scans of my JS files for common syntax and style errors.

Prettier scans my files for style issues and automatically reformats code to ensure consistent rules are being followed for spacing, indentation, indentation, single quotes vs double quotes, etc.

Using husky, lint-staged and precommit to run eslint and test on staged git files. This way failing changes can be caught early on. 

## Dependancy injection

Dependency injection is a technique in which an object receives other objects that it depends on, called dependencies

Instead of exporting an object, we export a function to make the object. The function states what dependencies it needs in order to do it’s thing.

I am using awilix to help with this. It allows you to create a container with all your dependencies and then it can be reused throughout your project.

## Logging

 Logs offer a way of persisting data about your application so you can view this data for analysis later.

 Winston package has more functionality far beyond console.log, it exposes different levels of logging and gives you control over where to send the logs. 

 ## Swagger api documentation


## Run with Docker

- Build the project `docker build . -t app` 
- Run the project `docker run -p 8080:9000 -d app` using 8080 thats forwarded to api port of 9000
- Make get request to localhost:8080/sample
## Running the project


## How to use