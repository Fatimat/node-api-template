FROM node:15.4

# Create app directory
WORKDIR /app

COPY package.json .
COPY yarn.lock .

# Install app dependencies
RUN yarn install

# Bundle app source
COPY . .

EXPOSE 9000

CMD ["yarn", "start"]